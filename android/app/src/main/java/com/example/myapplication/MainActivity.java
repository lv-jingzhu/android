package com.example.myapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public int Sign_RequestCode=1;
    public int Login_RequestCode=1;
Button sign;
Button login;
EditText name;
EditText psw;
String _name ,_psw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sign=findViewById(R.id.sign);
        login=findViewById(R.id.login);
        name=findViewById(R.id.name);
        psw=findViewById(R.id.psw);
        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent();
                intent.setClass(MainActivity.this,Sign.class);
                startActivityForResult(intent,Sign_RequestCode);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_name.equals(name.getText().toString())&&
                        _psw.equals(psw.getText().toString())){
                    Toast.makeText(MainActivity.this,"登陆成功",Toast.LENGTH_SHORT).show();
                    Intent intent =new Intent();
                    intent.setClass(MainActivity.this,denglu.class);
                    startActivityForResult(intent,Login_RequestCode);
                }else {
                    Toast.makeText(MainActivity.this,"登陆失败，账号或密码错误",Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK){
            String name1=data.getStringExtra("name");
            String psw1=data.getStringExtra("psw");
            name.setText(name1);
            name.setText(psw1);
            _name=name1;
            _psw=psw1;
        }
    }
}