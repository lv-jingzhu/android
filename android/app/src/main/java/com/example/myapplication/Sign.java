package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Sign extends AppCompatActivity {

    Button back;
    Button submit;
    EditText name;
    EditText psw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        back =findViewById(R.id.back);
        submit =findViewById(R.id.submit);
        name =findViewById(R.id.sign_name);
        psw =findViewById(R.id.sign_psw);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Sign.this,"提交成功",Toast.LENGTH_SHORT).show();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("name",name.getText().toString());
                intent.putExtra("psw",name.getText().toString());
                setResult(RESULT_OK,intent);
                finish();
            }
        });

    }
}